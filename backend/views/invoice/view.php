<?php
/** @var Info $model */
/**
 *
 * Created by FesVPN.
 * @project Default (Template) Project
 * @author  Pham Hai
 * @email   mitto.hai.7356@gmail.com
 * @date    8/3/2021
 * @time    10:28 PM
 */
/* @var $this \yii\web\View */
/** @var Info $info */
/** @var InvoiceFood $invoice_food */

use common\models\Info;
use common\models\InvoiceFood;
use yii\helpers\Html;


?>

<div class="main-content-inner">
	<div class="page-content">
		<div class="ace-settings-container" id="ace-settings-container">
			<div class="ace-settings-box clearfix" id="ace-settings-box">
				<div class="pull-left width-50">
					<div class="ace-settings-item">
						<div class="pull-left">
							<select id="skin-colorpicker" class="hide">
								<option data-skin="no-skin" value="#438EB9">#438EB9</option>
								<option data-skin="skin-1" value="#222A2D">#222A2D</option>
								<option data-skin="skin-2" value="#C6487E">#C6487E</option>
								<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
							</select><div class="dropdown dropdown-colorpicker">		<a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="btn-colorpicker" style="background-color:#438EB9"></span></a><ul class="dropdown-menu dropdown-caret"><li><a class="colorpick-btn selected" href="#" style="background-color:#438EB9;" data-color="#438EB9"></a></li><li><a class="colorpick-btn" href="#" style="background-color:#222A2D;" data-color="#222A2D"></a></li><li><a class="colorpick-btn" href="#" style="background-color:#C6487E;" data-color="#C6487E"></a></li><li><a class="colorpick-btn" href="#" style="background-color:#D0D0D0;" data-color="#D0D0D0"></a></li></ul></div>
						</div>
						<span>&nbsp; Choose Skin</span>
					</div>

					<div class="ace-settings-item">
						<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar">
						<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
					</div>

					<div class="ace-settings-item">
						<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar">
						<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
					</div>

					<div class="ace-settings-item">
						<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs">
						<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
					</div>

					<div class="ace-settings-item">
						<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl">
						<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
					</div>

					<div class="ace-settings-item">
						<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container">
						<label class="lbl" for="ace-settings-add-container">
							Inside
							<b>.container</b>
						</label>
					</div>
				</div><!-- /.pull-left -->

				<div class="pull-left width-50">
					<div class="ace-settings-item">
						<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover">
						<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
					</div>

					<div class="ace-settings-item">
						<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact">
						<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
					</div>

					<div class="ace-settings-item">
						<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight">
						<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
					</div>
				</div><!-- /.pull-left -->
			</div><!-- /.ace-settings-box -->
		</div><!-- /.ace-settings-container -->

		<div class="row">
			<div class="col-xs-12">
				<!-- PAGE CONTENT BEGINS -->
				<div class="space-6"></div>

				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="widget-box transparent">
							<div class="widget-header widget-header-large">
								<h3 class="widget-title grey lighter">
									<i class="ace-icon fa fa-leaf red"></i>
									Customer Invoice <?=$model->id?>
								</h3>

								<div class="widget-toolbar no-border invoice-info">
									<span class="invoice-info-label">Invoice:</span>
									<span class="red">#<?=$model->token?></span>

									<br>
									<span class="invoice-info-label">Date:</span>
									<span class="blue"><?=date('Y-m-d H:i:s',$model->created_at)?></span>
								</div>

								<div class="widget-toolbar hidden-480">
									<a href="#">
										<i class="ace-icon fa fa-print"></i>
									</a>
								</div>
							</div>
							<div class="widget-body">
								<div class="widget-main padding-24">
									<div class="row">
										<div class="col-sm-6">
											<div class="row">
												<div class="col-xs-11 label label-lg label-success arrowed-in arrowed-right">
													<b>Order info</b>
												</div>
											</div>

											<div>
												<ul class="list-unstyled spaced">
													<li>
														<i class="ace-icon fa fa-caret-right green"></i>Invoice No:<?=$model->id?>
													</li>

													<li>
														<i class="ace-icon fa fa-caret-right green"></i>Invoice token:<?=$model->token?>
													</li>

													<li>
														<i class="ace-icon fa fa-caret-right green"></i>Invoice status: <?=$model->status?>
													</li>

													<li>
														<i class="ace-icon fa fa-caret-right green"></i>
														Ordered Date:<?=date('Y-m-d H:i:s',$model->created_at)?>
													</li>

													<li class="divider"></li>
												</ul>
											</div>
										</div><!-- /.col -->

										<div class="col-sm-6">
											<div class="row">
												<div class="col-xs-11 label label-lg label-info arrowed-in arrowed-right">
													<b>Customer Info</b>
												</div>
											</div>

											<div>
												<ul class="list-unstyled  spaced">
													<li>
														<i class="ace-icon fa fa-caret-right blue"></i>Full name: <?=$info[0]->full_name?>
													</li>

													<li>
														<i class="ace-icon fa fa-caret-right blue"></i>Address:<?=$info[0]->address?>
													</li>

													<li>
														<i class="ace-icon fa fa-caret-right blue"></i>Email:<?=$info[0]->email?>
													</li>
													<li>
														<i class="ace-icon fa fa-caret-right blue"></i>Phone: <?=$info[0]->phone?>
													</li>

												</ul>
											</div>
										</div><!-- /.col -->
									</div><!-- /.row -->

									<div class="space"></div>

									<div>
										<table class="table table-striped table-bordered">
											<thead>
											<tr>
												<th class="center">#</th>
												<th>Menu's name</th>
												<th class="hidden-xs">Food's name</th>
												<th class="hidden-480">Quantity</th>
												<th>Price</th>
												<th>Sub Total</th>
											</tr>
											</thead>

											<tbody>
											<tr>
												<td class="center">1</td>

												<td>
													<?=$invoice_food[2]->food->menu->id?>
												</td>
												<td class="hidden-xs">
													<?=$invoice_food[2]->food->name?>
												</td>
												<td class="hidden-480"><?=$invoice_food[2]->quantity?> </td>
												<td>$<?=number_format($invoice_food[2]->price)?></td>
												<td>$<?=number_format($invoice_food[2]->amount)?></td>
											</tr>
											</tbody>
										</table>
									</div>

									<div class="hr hr8 hr-double hr-dotted"></div>

									<div class="row">
										<div class="col-sm-5 pull-right">
											<h4 class="pull-right">
												Total amount :
												<span class="red">$<?=number_format($invoice_food[2]->amount)?></span>
											</h4>
										</div>
									</div>
									<div class="button" style="text-align: center;">
										<?= Html::submitButton('Cancel this Invoice', ['class' => 'btn btn-danger']) ?>
										<?=Html::submitButton('Mark as Shipped',['class'=>'btn btn-primary'])?>
										<?=Html::submitButton('Mark as Delivered',['class'=>'btn btn-success'])?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.page-content -->
</div>
