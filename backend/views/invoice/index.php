<?php

use common\models\Info;
use common\models\Invoice;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoice-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Invoice', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
            		'attribute' => 'full_name',
	                'value' => function(Invoice $invoice){
    	                return Info::findOne(['invoice_id'=>$invoice->id])->full_name;
	                }

			],
            'total',
            'token',
            'type',
            'status',
            //'created_at',
            //'notify',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
