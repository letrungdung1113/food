<?php

use common\models\Info;
use common\models\Invoice;
use common\models\InvoiceFood;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\InvoiceFoodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Invoice Foods';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoice-food-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
	        [
		        'attribute' => 'Order ID',
		        'value' => function (InvoiceFood $invoiceFood) {
			        return  $invoiceFood->invoice->id;
		        },

	        ],
	        [
		        'attribute' => 'Order token',
		        'value' => function (InvoiceFood $invoiceFood) {
			        return  $invoiceFood->invoice->token;
		        },

	        ],
	        [
		        'attribute' => 'Customer Name',
		        'value' => function (InvoiceFood $invoiceFood) {
			        return  $invoiceFood->info->full_name;
		        },

	        ],
	        [
		        'attribute' => 'type',
		        'value' => function (InvoiceFood $invoiceFood) {
			        return  $invoiceFood->invoice->type;
		        },

	        ],
	        [
		        'attribute' => 'Total',
		        'value' => function (InvoiceFood $invoiceFood) {
			        return  '$'.number_format($invoiceFood->price);
		        },

	        ],
	        [
		        'attribute' => 'Status',
		        'value' => function (InvoiceFood $invoiceFood) {
			        return  $invoiceFood->invoice->status;
		        },

	        ],
//            'invoice_id',
//            'food_id',
//            'info_id',
//            'quantity',
            //'price',
            //'amount',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
