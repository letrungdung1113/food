<?php
/**
 * Created by Navatech.
 * @project food
 * @author  Phuong
 * @email   notteen[at]gmail.com
 * @date    10/3/2021
 * @time    8:40 AM
 */

namespace backend\controllers;
use yii\web\Controller;

class TestController extends Controller {
	public function actionIndex(){
		return $this->render('index');
	}
}
