<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "invoice".
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $total
 * @property int|null $token
 * @property string|null $type
 * @property string|null $status
 * @property int|null $created_at
 * @property int|null $notify
 * @property InvoiceFood[] $invoiceFoods
 * @property InvoiceTable[] $invoiceTables
 */
class Invoice extends \yii\db\ActiveRecord
{
	const STATUS_DRAFT = 'draft';
	const STATUS_PENDING = 'pending';
	const STATUS_COMPLETED = 'completed';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'total', 'token', 'created_at', 'notify'], 'integer'],
            [['status'], 'string'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'total' => 'Total',
            'token' => 'Token',
            'type' => 'Type',
            'status' => 'Status',
            'created_at' => 'Created At',
            'notify' => 'Notify',
        ];
    }

    /**
     * Gets query for [[InvoiceFoods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceFoods()
    {
        return $this->hasMany(InvoiceFood::className(), ['invoice_id' => 'id']);
    }
    public function getInvoiceInfos()
    {
        return $this->hasMany(Info::className(), ['invoice_id' => 'id']);
    }

    /**
     * Gets query for [[InvoiceTables]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceTables()
    {
        return $this->hasMany(InvoiceTable::className(), ['invoice_id' => 'id']);
    }
}
