<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "list_order".
 *
 * @property int $id
 * @property int|null $token
 * @property string $full_name
 * @property string|null $type
 * @property int|null $total
 * @property string|null $status
 * @property int|null $created_at
 */
class ListOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'list_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'token', 'total', 'created_at'], 'integer'],
            [['full_name'], 'required'],
            [['status'], 'string'],
            [['full_name', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
            'full_name' => 'Full Name',
            'type' => 'Type',
            'total' => 'Total',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }
}
